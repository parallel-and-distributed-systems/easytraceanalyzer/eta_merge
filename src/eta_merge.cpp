#include <eta/core/debug.hpp>
#include <eta/trace/loader.hpp>
#include <eta/core/ProgramOptions.hxx>
#include <eta/trace/otf2.hpp>

bool interleave_events=false;
bool verbose=false;
struct container_status  {
  const eta::Trace& t;
  eta::Container* cont;
  std::vector<eta::EventId>::iterator it;
};


void printContainer(const eta::Trace &t, const eta::Container &cont) {
  std::cout<<"\tContainer "<<cont.id<<" : "<<cont.name<<" : "<<cont.events.size()<<" events (parent:"<<cont.parent<<")"<<std::endl;
}

void printTrace(const eta::Trace& t) {
  std::cout<<"Trace info:"<<std::endl;
  std::cout<<"\t.name: "<<t.name<<std::endl;
  std::cout<<"\t.path: "<<t.path<<std::endl;
  std::cout<<"\t.type: "<<t.type<<std::endl;
  std::cout<<"\t.events: "<<t.events.size()<<std::endl;
  std::cout<<"\t.containers: "<<t.containers.size()<<std::endl;
  std::cout<<"\t.communications: "<<t.communications.size()<<std::endl;

  for(const auto& cont:t.containers ) {
    printContainer(t, cont);
  }
}

int main(int argc, char**argv) {
  bool verbose = false;
  bool merge = false;
  std::string output_filename = std::string("output/trace");
  auto parser = po::parser {};
  parser.verbose(std::cerr);
 
  auto & help_opt = parser["help"]
    .abbreviation('h')
    .description("display this help")
    .single();

  auto & verbose_opt = parser["verbose"]
    .abbreviation('v')
    .description("enables extra informations")
    .callback([]() {
      eta::set_log_level(eta::log_level_t::debug);
      eta::log_info("set debug level to 'debug'");
    })
    .single();

  auto & merge_opt = parser["merge"]
    .abbreviation('m')
    .description("Merge containers with similar names")
    .single();

  auto & output_opt = parser["output"]
    .abbreviation('o')
    .description("Select the output filename")
    .single()
    .type(po::string)
    .bind(output_filename);

  auto & input_files = parser[""]
    .description("Input trace files")
    .multi()
    .type(po::string);

  if(!parser(argc, argv)) {
    std::cerr << "errors occurred; aborting\n";
    return -1;
  }
  
  auto usage = [&]() {
    std::cout << parser;
  };

  // Active the options
  if (help_opt.was_set()) {
    usage();
    return EXIT_SUCCESS;
  }
  if(input_files.size() == 0) {
    usage();
    return EXIT_FAILURE;
  }
  if(verbose_opt.was_set()) { verbose = true; }
  if(merge_opt.was_set()) { merge = true; }


  
  std::vector<struct container_status> containers;
  std::vector<eta::Trace> traces;

  for (auto &trace_file: input_files) {
    if(verbose)
      std::cout<<"Opening "<<trace_file.string<<std::endl;
    auto trace = eta::loadTrace(eta::Path(trace_file.string));
    if(std::holds_alternative<eta::ParsingError>(trace)) {
      std::cerr<<"Error parsing "<<trace_file.string<<std::endl;
      return EXIT_FAILURE;
    }

    traces.push_back(std::move(std::get<eta::Trace>(trace)));
  }

  if(verbose) {
    std::cout<<"Merging "<<traces.size()<<" traces:\n";

    std::cout<<"Trace[0]:\n";
    printTrace(traces[0]);
  }

  for(int i=1; i<traces.size(); i++) {
    if(verbose) {
      std::cout<<"Trace["<<i<<"]:\n";
      printTrace(traces[i]);
    }
    traces[0].mergeTrace(traces[i], merge);
  }

  std::cout<<"Writing the resulting trace to "<<output_filename<<".otf2"<<std::endl;
  eta::otf2::writeTrace(traces[0], eta::Path(output_filename));

  return EXIT_SUCCESS;
}

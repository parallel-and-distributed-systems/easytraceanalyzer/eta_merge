cmake_minimum_required (VERSION 3.1)
INCLUDE (CheckLibraryExists)
find_package(PkgConfig)

project (eta_merge)
set (eta_merge_VERSION_MAJOR 0)
set (eta_merge_VERSION_MINOR 1)
set (eta_merge_PATCH_VERSION 0)

SET(PROJECT_DESCRIPTION "eta_merge is a command line tool for merging traces")

SET(PKG_CONFIG_LIBDIR  "\${prefix}/lib"  )
SET(PKG_CONFIG_INCLUDEDIR  "\${prefix}/include"  )
SET(PKG_CONFIG_LIBS  "-L\${libdir}"  )
SET(PKG_CONFIG_CFLAGS  "-I\${includedir}"  )

# use, i.e. don't skip the full RPATH for the build tree
set(CMAKE_SKIP_BUILD_RPATH FALSE)
# when building, don't use the install RPATH already
# (but later on when installing)
set(CMAKE_BUILD_WITH_INSTALL_RPATH FALSE)
set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
# add the automatically determined parts of the RPATH
# which point to directories outside the build tree to the install RPATH
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

# the RPATH to be used when installing, but only if it's not a system directory
list(FIND CMAKE_PLATFORM_IMPLICIT_LINK_DIRECTORIES "${CMAKE_INSTALL_PREFIX}/lib" isSystemDir)
if("${isSystemDir}" STREQUAL "-1")
    set(CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
endif("${isSystemDir}" STREQUAL "-1")

pkg_check_modules(ETA_CORE REQUIRED IMPORTED_TARGET eta_core)
pkg_check_modules(ETA_TRACE REQUIRED IMPORTED_TARGET eta_trace)

option(ENABLE_DEBUG "Enable Debug" ON)
if(ENABLE_DEBUG)
  set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O0 -g")
endif()

# configure a header file to pass some of the CMake settings
# to the source code
configure_file (
  "${PROJECT_SOURCE_DIR}/src/eta_merge.h.in"
  "${PROJECT_BINARY_DIR}/src/eta_merge.h"
  )

include_directories("${PROJECT_BINARY_DIR}/src")
include_directories("${PROJECT_SOURCE_DIR}/src")

include_directories(/opt/otf2/include)
link_directories(/opt/otf2/lib)

add_subdirectory ("src")

install(TARGETS DESTINATION bin)

CONFIGURE_FILE(
  "${CMAKE_CURRENT_SOURCE_DIR}/pkg-config.pc.cmake"
  "${CMAKE_CURRENT_BINARY_DIR}/${PROJECT_NAME}.pc"
  )

install(FILES "${CMAKE_BINARY_DIR}/${PROJECT_NAME}.pc"
          DESTINATION lib/pkgconfig)
